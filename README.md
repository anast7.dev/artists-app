
# Artists App

## Introduction

This README provides essential information about the Symfony 5 application.
It serves as a guide for developers, contributors, and users to understand, set up, and run the application.

It is a website showcasing paintings.

## Prerequisites

Before you begin, ensure you have met the following requirements:
- PHP = 7.4
- Composer installed
- Symfony CLI (optional but recommended)
- Docker
- Docker-compose

You can check the Prerequisites for Symfony CLI with the following command :

```bash
symfony check:requirements
````

## Installation

1. Clone the repository:
```bash
git clone https://github.com/yourusername/your-symfony-app.git
````

2. Change into the project directory :
```bash
cd your-symfony-app
````

3. Install project dependencies using Composer:
```bash
composer install
````

## Running development environment using Docker: 
```bash
docker-compose up -d
symfony serve -d
```

## Tests

```bash
php bin/phpunit --testdox
````