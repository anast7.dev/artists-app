<?php

namespace App\Tests;

use App\Entity\BlogPost;
use App\Entity\User;
use DateTime;
use PHPUnit\Framework\TestCase;

class BlogPostTest extends TestCase
{
    public function testIsTrue()
    {
        $blogPost = new BlogPost();
        $user = new User();

        $datetime = new DateTime();

        $blogPost->setTitle('title')
            ->setContent('content')
            ->setCreateAt($datetime)
            ->setSlug('slug')
            ->setUser($user);

        $this->assertTrue($blogPost->getTitle() === "title");
        $this->assertTrue($blogPost->getContent() === "content");
        $this->assertTrue($blogPost->getCreateAt() === $datetime);
        $this->assertTrue($blogPost->getSlug() === 'slug');
        $this->assertTrue($blogPost->getUser() === $user);
    }

    public function testIsFalse()
    {
        $blogPost = new BlogPost();
        $user = new User();

        $datetime = new DateTime();

        $blogPost->setTitle('title')
            ->setContent('content')
            ->setCreateAt($datetime)
            ->setSlug('slug')
            ->setUser($user);

        $this->assertFalse($blogPost->getTitle() == "false");
        $this->assertFalse($blogPost->getContent() == "false");
        $this->assertFalse($blogPost->getCreateAt() == new DateTime());
        $this->assertFalse($blogPost->getSlug() === 'false');
        $this->assertFalse($blogPost->getUser() === new User());
    }

    public function testIsEmpty()
    {
        $blogPost = new BlogPost();

        $this->assertEmpty($blogPost->getTitle());
        $this->assertEmpty($blogPost->getContent());
        $this->assertEmpty($blogPost->getCreateAt());
        $this->assertEmpty($blogPost->getSlug());
        $this->assertEmpty($blogPost->getUser());
    }
}
