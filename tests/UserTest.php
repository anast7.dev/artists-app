<?php

namespace App\Tests;

use App\Entity\User;
use PHPUnit\Framework\TestCase;

class UserTest extends TestCase
{
    public function testIsTrue(): void
    {
        $user = new User();

        $user->setEmail('true@test.com')
            ->setFirstName("Firstname")
            ->setLastName("Lastname")
            ->setPassword("password")
            ->setAbout("About")
            ->setInstagram("Instagram");

        $this->assertTrue($user->getEmail() === 'true@test.com');
        $this->assertTrue($user->getFirstName() === 'Firstname');
        $this->assertTrue($user->getLastName() === 'Lastname');
        $this->assertTrue($user->getPassword() === 'password');
        $this->assertTrue($user->getAbout() === 'About');
        $this->assertTrue($user->getInstagram() === 'Instagram');
    }

    public function testIsFalse(): void
    {
        $user = new User();

        $user->setEmail('true@test.com')
            ->setFirstName("Firstname")
            ->setLastName("Lastname")
            ->setPassword("password")
            ->setAbout("About")
            ->setInstagram("Instagram");

        $this->assertFalse($user->getEmail() === 'false@test.com');
        $this->assertFalse($user->getFirstName() === 'false');
        $this->assertFalse($user->getLastName() === 'false');
        $this->assertFalse($user->getPassword() === 'false');
        $this->assertFalse($user->getAbout() === 'false');
        $this->assertFalse($user->getInstagram() === 'false');
    }

    public function testIsEmpty(): void
    {
        $user = new User();

        $this->assertEmpty($user->getEmail());
        $this->assertEmpty($user->getFirstName());
        $this->assertEmpty($user->getLastName());
        $this->assertEmpty($user->getPassword());
        $this->assertEmpty($user->getAbout());
        $this->assertEmpty($user->getInstagram());
    }
}
