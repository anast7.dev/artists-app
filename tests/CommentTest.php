<?php

namespace App\Tests;

use App\Entity\BlogPost;
use App\Entity\Comment;
use App\Entity\Painting;
use DateTime;
use PHPUnit\Framework\TestCase;

class CommentTest extends TestCase
{
    public function testIsTrue()
    {
        $comment = new Comment();
        $blogPost = new BlogPost();
        $painting = new Painting();

        $datetime = new DateTime();

        $comment->setAuthor("author")
            ->setEmail("email@test.com")
            ->setContent("content")
            ->setCreateAt($datetime)
            ->setBlogPost($blogPost)
            ->setPainting($painting);

        $this->assertTrue($comment->getAuthor() === "author");
        $this->assertTrue($comment->getEmail() === "email@test.com");
        $this->assertTrue($comment->getContent() === "content");
        $this->assertTrue($comment->getCreateAt() === $datetime);
        $this->assertTrue($comment->getBlogPost() === $blogPost);
        $this->assertTrue($comment->getPainting() === $painting);

    }

    public function testIsFalse()
    {
        $comment = new Comment();
        $blogPost = new BlogPost();
        $painting = new Painting();

        $datetime = new DateTime();

        $comment->setAuthor("author")
            ->setEmail("email@test.com")
            ->setContent("content")
            ->setCreateAt($datetime)
            ->setBlogPost($blogPost)
            ->setPainting($painting);

        $this->assertFalse($comment->getAuthor() === "false");
        $this->assertFalse($comment->getEmail() === "false@test.com");
        $this->assertFalse($comment->getContent() === "false");
        $this->assertFalse($comment->getCreateAt() === new DateTime());
        $this->assertFalse($comment->getBlogPost() === new BlogPost());
        $this->assertFalse($comment->getPainting() === new Painting());

    }

    public function testIsEmpty()
    {
        $comment = new Comment();

        $this->assertEmpty($comment->getAuthor());
        $this->assertEmpty($comment->getEmail());
        $this->assertEmpty($comment->getContent());
        $this->assertEmpty($comment->getCreateAt());
        $this->assertEmpty($comment->getBlogPost());
        $this->assertEmpty($comment->getPainting());
    }
}
