<?php

namespace App\Tests;

use App\Entity\Category;
use PHPUnit\Framework\TestCase;

class CategoryTest extends TestCase
{
    public function testIsTrue() {

        $category = new Category();

        $category->setName("Name")
            ->setDescription("Description")
            ->setSlug("Slug");

        $this->assertTrue($category->getName() === "Name");
        $this->assertTrue($category->getDescription() === "Description");
        $this->assertTrue($category->getSlug() === "Slug");

    }

    public function testIsFalse() {

        $category = new Category();

        $category->setName("Name")
            ->setDescription("Description")
            ->setSlug("Slug");

        $this->assertFalse($category->getName() === "false");
        $this->assertFalse($category->getDescription() === "false");
        $this->assertFalse($category->getSlug() === "false");

    }

    public function testIsEmpty() {

        $category = new Category();


        $this->assertEmpty($category->getName());
        $this->assertEmpty($category->getDescription());
        $this->assertEmpty($category->getSlug());

    }
}
