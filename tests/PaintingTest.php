<?php

namespace App\Tests;

use App\Entity\Category;
use App\Entity\Painting;
use App\Entity\User;
use DateTime;
use PHPUnit\Framework\TestCase;

class PaintingTest extends TestCase
{
    public function testIsTrue()
    {
        $painting = new Painting();
        $category = new Category();
        $user = new User();

        $datetime = new DateTime();

        $painting->setName("Name")
            ->setWidth(20.20)
            ->setHeight(20.20)
            ->setOnSale(true)
            ->setCompletionDate($datetime)
            ->setCreatedAt($datetime)
            ->setDescription("Description")
            ->setPortfolio(true)
            ->setSlug('slug')
            ->setFile('file')
            ->setPrice(20.20)
            ->setUser($user)
            ->addCategory($category);

        $this->assertTrue($painting->getName() === "Name");
        $this->assertTrue($painting->getWidth() == 20.20);
        $this->assertTrue($painting->getHeight() == 20.20);
        $this->assertTrue($painting->isOnSale() === true);
        $this->assertTrue($painting->getCompletionDate() === $datetime);
        $this->assertTrue($painting->getCreatedAt() === $datetime);
        $this->assertTrue($painting->getDescription() === "Description");
        $this->assertTrue($painting->isPortfolio() === true);
        $this->assertTrue($painting->getSlug() === 'slug');
        $this->assertTrue($painting->getFile() === 'file');
        $this->assertTrue($painting->getPrice() == 20.20);
        $this->assertTrue($painting->getUser() === $user);
        $this->assertContains($category, $painting->getCategory());

    }

    public function testIsFalse()
    {
        $painting = new Painting();
        $category = new Category();
        $user = new User();

        $datetime = new DateTime();

        $painting->setName("Name")
            ->setWidth(20.20)
            ->setHeight(20.20)
            ->setOnSale(true)
            ->setCompletionDate($datetime)
            ->setCreatedAt($datetime)
            ->setDescription("Description")
            ->setPortfolio(true)
            ->setSlug('slug')
            ->setFile('file')
            ->setPrice(20.20)
            ->setUser($user)
            ->addCategory($category);

        $this->assertFalse($painting->getName() === "false");
        $this->assertFalse($painting->getWidth() == 22.20);
        $this->assertFalse($painting->getHeight() == 22.20);
        $this->assertFalse($painting->isOnSale() === false);
        $this->assertFalse($painting->getCompletionDate() === new DateTime());
        $this->assertFalse($painting->getCreatedAt() === new DateTime());
        $this->assertFalse($painting->getDescription() === "false");
        $this->assertFalse($painting->isPortfolio() === false);
        $this->assertFalse($painting->getSlug() === 'false');
        $this->assertFalse($painting->getFile() === 'false');
        $this->assertFalse($painting->getPrice() == 22.20);
        $this->assertFalse($painting->getUser() === new User());
        $this->assertNotContains(new Category(), $painting->getCategory());

    }

    public function testIsEmpty()
    {
        $painting = new Painting();

        $this->assertEmpty($painting->getName());
        $this->assertEmpty($painting->getWidth());
        $this->assertEmpty($painting->getHeight());
        $this->assertEmpty($painting->isOnSale());
        $this->assertEmpty($painting->getCompletionDate());
        $this->assertEmpty($painting->getCreatedAt());
        $this->assertEmpty($painting->getDescription());
        $this->assertEmpty($painting->isPortfolio());
        $this->assertEmpty($painting->getSlug());
        $this->assertEmpty($painting->getFile());
        $this->assertEmpty($painting->getPrice());
        $this->assertEmpty($painting->getUser());
        $this->assertEmpty($painting->getCategory());
    }
}
