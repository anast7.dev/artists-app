<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230911123933 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE painting_category (painting_id INT NOT NULL, category_id INT NOT NULL, INDEX IDX_79D2014EB00EB939 (painting_id), INDEX IDX_79D2014E12469DE2 (category_id), PRIMARY KEY(painting_id, category_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE painting_category ADD CONSTRAINT FK_79D2014EB00EB939 FOREIGN KEY (painting_id) REFERENCES painting (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE painting_category ADD CONSTRAINT FK_79D2014E12469DE2 FOREIGN KEY (category_id) REFERENCES category (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE painting ADD user_id INT NOT NULL');
        $this->addSql('ALTER TABLE painting ADD CONSTRAINT FK_66B9EBA0A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('CREATE INDEX IDX_66B9EBA0A76ED395 ON painting (user_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE painting_category DROP FOREIGN KEY FK_79D2014EB00EB939');
        $this->addSql('ALTER TABLE painting_category DROP FOREIGN KEY FK_79D2014E12469DE2');
        $this->addSql('DROP TABLE painting_category');
        $this->addSql('ALTER TABLE painting DROP FOREIGN KEY FK_66B9EBA0A76ED395');
        $this->addSql('DROP INDEX IDX_66B9EBA0A76ED395 ON painting');
        $this->addSql('ALTER TABLE painting DROP user_id');
    }
}
